Source: libcloudflare-client-perl
Section: perl
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: libhttp-message-perl,
 libindirect-perl,
 libipc-system-simple-perl,
 libjson-any-perl,
 libjson-maybexs-perl,
 libkavorka-perl,
 liblwp-protocol-https-perl (>= 6.02),
 libmoose-perl,
 libmoosex-strictconstructor-perl,
 libnamespace-autoclean-perl,
 libreadonly-perl,
 libtest-cpan-meta-json-perl,
 libtest-exception-perl,
 libtest-lwp-useragent-perl,
 libtest-requiresinternet-perl,
 libthrowable-perl,
 libtry-tiny-perl,
 libtype-tiny-perl,
 libwww-perl (>= 6.02),
 perl
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcloudflare-client-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcloudflare-client-perl.git
Homepage: https://metacpan.org/release/CloudFlare-Client
Testsuite: autopkgtest-pkg-perl

Package: libcloudflare-client-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends},
 libindirect-perl,
 libjson-maybexs-perl,
 libkavorka-perl,
 liblwp-protocol-https-perl (>= 6.02),
 libmoose-perl,
 libmoosex-strictconstructor-perl,
 libnamespace-autoclean-perl,
 libreadonly-perl,
 libthrowable-perl,
 libtype-tiny-perl,
 libwww-perl (>= 6.02)
Description: object-orientated interface to the CloudFlare client API
 CloudFlare::Client provides an object-orientated interface to the CloudFlare
 client API, as described at https://www.cloudflare.com/docs/client-api.html
 .
 API actions are mapped to methods of the same name and arguments are passed in
 as a hash with keys as given in the docs. Successful API calls return the
 response section from the upstream JSON API. Failures for whatever reason
 throw exceptions under the CloudFlare::Client::Exception:: namespace.
